# training-k8s

Kubernetes training files.

## Usages

This repository contains shellscripts and YAML files for practical exercices
regarding Kubernetes training course.

## License

All codes are under BSD-3 license.

