#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# from https://gist.github.com/d-a-n/baabf3b010a6851f0e84
### A simple helper script for using ansible and vagrant together.
# Usually you don't need an inventory file for vagrant, since one is created
# automatically. But if you want to give your vagrant host a special group
# or assign some variables, this script becomes handy.
#
# Use it like this:
#    1) create a file e.g ansible/inventories/vagrant.py and paste the content of this gist
#    2) give the file execution permissions: chmod +x ansible/inventories/vagrant.py
#    3) open your Vagrantfile and change it accordingly:
#
#    config.vm.provision "ansible" do |ansible|
#        [..]
#        ansible.inventory_path = "ansible/inventories/vagrant.py"
#        [..]
#
#    end

import json, subprocess
from jinja2 import Template, Environment, FileSystemLoader

def get_vagrant_sshconfig():
    p = subprocess.Popen("vagrant ssh-config", stdout=subprocess.PIPE, shell=True)
    raw = p.communicate()[0].decode('utf-8')
    sshconfig = {}
    lines = raw.split("\n")
    host = '';
    for line in lines:
        kv = line.strip().split(" ", 1)
        if len(kv) == 2:
            if kv[0] == 'Host':
                host = kv[1];
                sshconfig[host] = dict()
            elif host != '' :
                sshconfig[host][kv[0]] = kv[1]
    return sshconfig

sshconfig = get_vagrant_sshconfig()

#print(sshconfig.get('k8s-master-1'))
masters = {}
workers = {}

for e in sshconfig:
    if e.startswith('k8s-master'):
        masters[e] = sshconfig[e]
    else:
        workers[e] = sshconfig[e]

print(masters)
print(workers)
file_loader = FileSystemLoader("template")
env = Environment(loader=file_loader)

template = env.get_template('hosts.ini.j2')
output = template.render(masters = masters, workers = workers)

print(output)

with open('../inventory/hosts.ini', 'w') as file:
    file.write(output)

