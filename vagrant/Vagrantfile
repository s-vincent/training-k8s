# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

image = "almalinux/9"
nb_master = 1
nb_worker = 2
ram = 3584
bastion_ram = 1024
vcpu = 4

Vagrant.configure("2") do |config|
  # Basic setup
  config.vm.provision "file", source: "~/.vagrant.d/insecure_private_key",
    destination: "~/.ssh/id_rsa"
  config.vm.provision 'Setup', type: 'shell',
    path: "provisioning/setup.sh"

  # Don't regenerate a new SSH key for each VM
  # Use ~/.vagrant.d/insecure_private_key
  config.ssh.insert_key = false

  # Disable default shared folder
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Disable default SSH
  config.vm.network "forwarded_port", guest: 22, host: 2222, id: "ssh", disabled: true

  # Configure bastion VM
  config.vm.define "k8s-bastion" do |host|
    host.vm.box = image
    # if we store image locally, pass the URL
    #host.vm.box_url = image_url
    host.vm.hostname = "k8s-bastion"
    host.vm.network :private_network, ip: "10.42.0.242"
    host.vm.network "forwarded_port", guest: 22, host: (2242), host_ip: "127.0.0.1"

    # Virtualbox specific configuration
    host.vm.provider "virtualbox" do |v|
      v.memory = bastion_ram
      v.cpus = vcpu
    end
    # libvirt specific configuration
    host.vm.provider "libvirt" do |l|
      l.qemu_use_session = false
      l.memory = bastion_ram
      l.cpus = vcpu
    end
    # VMware workstation
    host.vm.provider "vmware_desktop" do |v|
      v.vmx["memsize"] = bastion_ram
      v.vmx["numvcpus"] = vcpu
    end
  end

  # Configure master VMs
  (1..nb_master).each do |i|
    config.vm.define "k8s-master-#{i}" do |host|
      host.vm.box = image
      # if we store image locally, pass the URL
      #host.vm.box_url = image_url
      host.vm.hostname = "k8s-master-#{i}"
      host.vm.network :private_network, ip: "10.42.0.#{10 + i}"
      host.vm.network "forwarded_port", guest: 22, host: (2200 + i), host_ip: "127.0.0.1"

      # Virtualbox specific configuration
      host.vm.provider "virtualbox" do |v|
        v.memory = ram
        v.cpus = vcpu
      end
      # libvirt specific configuration
      host.vm.provider "libvirt" do |l|
        l.qemu_use_session = false
        l.memory = ram
        l.cpus = vcpu
      end
      # VMware workstation
      host.vm.provider "vmware_desktop" do |v|
        v.vmx["memsize"] = ram
        v.vmx["numvcpus"] = vcpu
      end
    end
  end

  # Configure worker VMs
  (1..nb_worker).each do |i|
    config.vm.define "k8s-worker-#{i}" do |host|
      host.vm.box = image
      # if we store image locally, pass the URL
      #host.vm.box_url = image_url
      host.vm.hostname = "k8s-worker-#{i}"
      host.vm.network :private_network, ip: "10.42.0.#{20 + i}"
      host.vm.network "forwarded_port", guest: 22, host: (2210 + i), host_ip: "127.0.0.1"

      # Virtualbox specific configuration
      host.vm.provider "virtualbox" do |v|
        v.memory = ram
        v.cpus = vcpu
      end
      # libvirt specific configuration
      host.vm.provider "libvirt" do |l|
        l.qemu_use_session = false
        l.memory = ram
        l.cpus = vcpu
      end
      # VMware workstation
      host.vm.provider "vmware_desktop" do |v|
        v.vmx["memsize"] = ram
        v.vmx["numvcpus"] = vcpu
      end
    end
  end
end

