#!/bin/sh

echo "Provisioning virtual machine..."

echo "Configure localtime"
if [ -z $TZ ]; then
  TZ="Europe/Paris"
fi

cat <<EOF | tee /etc/environment
LANG=en_US.utf-8
LC_ALL=en_US.utf-8
EOF

rm /etc/localtime
ln -sf /usr/share/zoneinfo/${TZ} /etc/localtime

echo "Configure access to root account via public key"
mkdir -p /root/.ssh
cp /home/vagrant/.ssh/authorized_keys /root/.ssh/

echo "Adds alma user"
adduser alma || true
mkdir -p /home/alma/.ssh
cp /home/vagrant/.ssh/authorized_keys /home/alma/.ssh/
chown -R alma:alma /home/alma/.ssh
echo "%alma ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/alma

echo "Adds additionnal packages"
yum install -y glibc-langpack-en

# install kubernetes client stuff on bastion
if [ "$(hostname)" = "k8s-bastion" ]; then
  yum install -y curl vim bash-completion
  curl -L https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl --output /usr/local/bin/kubectl
  chmod +x /usr/local/bin/kubectl
  curl -L https://github.com/jonmosco/kube-ps1/archive/v0.9.0.tar.gz | tar xzv -C /usr/local/bin/ --strip=1 --wildcards kube*/kube-ps1.sh
  curl -L https://github.com/ahmetb/kubectx/releases/download/v0.9.5/kubectx_v0.9.5_linux_x86_64.tar.gz | tar xzv -C /usr/local/bin/ --wildcards kubectx
  curl -L https://github.com/ahmetb/kubectx/releases/download/v0.9.5/kubens_v0.9.5_linux_x86_64.tar.gz | tar xzv -C /usr/local/bin/ --wildcards kubens
  curl -L https://get.helm.sh/helm-v3.15.2-linux-amd64.tar.gz | tar xzv -C /usr/local/bin/ --strip=1 --wildcards linux*/helm
  curl -L https://github.com/derailed/k9s/releases/download/v0.32.5/k9s_Linux_amd64.tar.gz | tar xzv -C /usr/local/bin --wildcards k9s
  curl -LO https://training.vincent-netsys.fr/ube/install-kubespray.sh
  chown vagrant:vagrant install-kubespray.sh
  chmod +x install-kubespray.sh

  mkdir /home/vagrant/.kube
  chown -R vagrant:vagrant /home/vagrant/.kube

  cat <<EOF >> /home/vagrant/.bashrc
source /usr/local/bin/kube-ps1.sh
PS1='\u@\h:\w\$(kube_ps1)\$ '
alias k='/usr/local/bin/kubectl'
alias kcx='/usr/local/bin/kubectx'
alias kns='/usr/local/bin/kubens'
source <(kubectl completion bash)
source <(kubectl completion bash | sed s/kubectl/k/g)
kubeon
EOF

  cat <<EOF >> /etc/hosts
10.42.0.11 k8s-master-1
10.42.0.12 k8s-master-2
10.42.0.13 k8s-master-3
10.42.0.21 k8s-worker-1
10.42.0.22 k8s-worker-2
10.42.0.23 k8s-worker-3
EOF

fi

echo "Done"

