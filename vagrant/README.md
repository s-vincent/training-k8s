# Vagrant setup

## Requirements

Install vagrant.

## VM

* k8s-bastion (10.42.0.242)
* k8s-master-1 (10.42.0.11)
* k8s-worker-1 (10.42.0.21)
* k8s-worker-2 (10.42.0.22)

## Start VM

```
vagrant up <name>
```

## Destroy VM

```
vagrant destroy <name> -f
```

## Connection

```
vagrant ssh <name>
```

