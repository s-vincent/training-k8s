#!/bin/sh

kubectl create deploy --image=svincent/go-hello:1.0 go-hello
kubectl create deploy --image=svincent/go-hello:2.0 go-hello-2
kubectl create deploy --image=svincent/go-hello:probes go-hello-probes
kubectl create deploy --image=redis:alpine redis
kubectl create deploy --image=svincent/cpu:1.0 cpu
kubectl create deploy --image=alpine:3.18 first

sleep 30

kubectl delete deploy go-hello
kubectl delete deploy go-hello-2
kubectl delete deploy go-hello-probes
kubectl delete deploy redis
kubectl delete deploy cpu
kubectl delete deploy first

