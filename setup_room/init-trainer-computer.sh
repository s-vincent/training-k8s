#!/bin/sh

set -e

sudo yum install ansible-core -y
sudo yum install ansible-collection-ansible-posix ansible-collection-community-general -y || echo
ssh-keygen -t rsa -f ~/.ssh/id_rsa -N "" -q
