#!/bin/sh

VERSION=1.35.0

wget -c https://github.com/kubernetes/minikube/releases/download/v${VERSION}/docker-machine-driver-kvm2
wget -c https://github.com/kubernetes/minikube/releases/download/v${VERSION}/minikube-linux-amd64

sudo install minikube-linux-amd64 /usr/local/bin/minikube
sudo install docker-machine-driver-kvm2 /usr/local/bin
minikube kubectl
sudo install  $(find ~/.minikube/| grep kubectl | sort -r | head -n1) /usr/local/bin/

rm -f minikube-linux-amd64 docker-machine-driver-kvm2

