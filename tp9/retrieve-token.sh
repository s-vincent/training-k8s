#!/bin/sh

if [ $# -lt 1 ]; then
  echo "Usage: $0 secret-name"
  exit 1
fi

kubectl get secret $1 -n kube-system -o jsonpath='{.data.token}' | base64 -d ; echo
