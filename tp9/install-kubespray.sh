sudo yum install python3.11 python3.11-pip vim -y
curl -L -O https://framagit.org/s-vincent/training-k8s/-/archive/master/training-k8s-master.tar.gz
tar -xzvf training-k8s-master.tar.gz

cd /home/vagrant/training-k8s-master/tp9
curl -L -O https://github.com/kubernetes-sigs/kubespray/archive/refs/tags/v2.26.0.tar.gz
tar -xzvf v2.26.0.tar.gz
python3.11 -m pip install -r kubespray-2.26.0/requirements.txt --use-pep517
cd kubespray-2.26.0 && ansible-playbook -i ../inventory/hosts.ini -u alma -b ./cluster.yml

