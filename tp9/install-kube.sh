#!/bin/sh

DIR=$(dirname $0)

ansible-playbook -i ${DIR}/inventory/hosts.ini -u centos -b $* \
   ${DIR}/kubespray-2.17.0/cluster.yml $* 2>&1 | tee ${DIR}/install.log

